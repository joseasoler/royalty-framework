# Royalty Framework

[![RimWorld](https://img.shields.io/badge/RimWorld-1.3-informational)](https://rimworldgame.com/) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) [![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](code_of_conduct.md)

This [RimWorld](https://rimworldgame.com) mod provides a better experience when multiple factions that use the Honor system are present. It requires the [Royalty DLC](https://rimworldgame.com/royalty/) and the [Harmony mod](https://steamcommunity.com/workshop/filedetails/?id=2009463077).

### Improved permits window

Without this mod the permits window will show all factions (even those that cannot award titles) and will always preselect the Empire even if the chosen pawn does not have one of their titles.

The permits window will now only show factions in which the pawn has a title, and will preselect a faction in which they have a title.

## Contributions

This project encourages community involvement and contributions. Check the [CONTRIBUTING](CONTRIBUTING.md) file for details. Existing contributors can be checked in the [contributors list](https://gitlab.com/joseasoler/royalty-framework/-/graphs/main).

## License

This project is licensed under the MIT license. Check the [LICENSE](LICENSE) file for details.

This mod and all contributions to it must comply with the terms of the [Rimworld End-User License Agreement](https://store.steampowered.com/eula/294100_eula_1).

## Acknowledgements

Read the [ACKNOWLEDGEMENTS.md](ACKNOWLEDGEMENTS.md) file for details.
