using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using HarmonyLib;
using RimWorld;
using UnityEngine;
using Verse;
using Verse.Sound;

namespace RoyaltyFramework.Harmony
{
	/// <summary>
	/// Improved Permits tab window. Uses a destructive prefix because the required changes are too extensive to reuse
	/// most of the existing code.
	/// </summary>
	[StaticConstructorOnStartup]
	[HarmonyPatch(typeof(PermitsCardUtility), "DrawRecordsCard")]
	public class PermitsCardUtility_DrawRecordsCard
	{
		/// <summary>
		/// Switch faction button icon.
		/// </summary>
		private static readonly Texture2D SwitchFactionIcon = ContentFinder<Texture2D>.Get("UI/Icons/SwitchFaction");

		/// <summary>
		/// Percentage of the window width used for showing the selected permit information.
		/// </summary>
		private const float LeftRectPercent = 0.33f;

		/// <summary>
		/// Calculates the honor cost of returning all of the permits of a pawn. 
		/// </summary>
		/// <param name="pawn">Pawn that desires to return permits.</param>
		/// <returns>Total cost of returning all permits.</returns>
		public static int TotalReturnPermitsCost(Pawn pawn)
		{
			// ToDo Customizable per faction.
			var num = 8;
			// The function seems to return permits from all factions, which is really weird. For now this code will not be
			// modified.
			var allFactionPermits = pawn.royalty.AllFactionPermits;
			foreach (var factionPermit in allFactionPermits)
			{
				if (factionPermit.OnCooldown && factionPermit.Permit.royalAid != null)
					num += factionPermit.Permit.royalAid.favorCost;
			}

			return num;
		}

		/// <summary>
		/// Draws the switch faction button if required, and generates its faction menu.
		/// </summary>
		/// <param name="titles">Titles held by the pawn.</param>
		/// <param name="rect">Rectangle area in which the button will be placed.</param>
		public static void SwitchFactionButton(List<RoyalTitle> titles, Rect rect)
		{
			// The switch faction button should only appear if the pawn holds titles in more than one faction.
			if (titles.Count <= 1)
			{
				return;
			}

			var factionButtonRect = new Rect(rect.x, rect.y, 32f, 32f);
			if (Widgets.ButtonImage(factionButtonRect, SwitchFactionIcon))
			{
				var options = new List<FloatMenuOption>();
				foreach (var title in titles)
				{
					var faction = title.faction;
					if (faction.IsPlayer || faction.def.permanentEnemy) continue;
					options.Add(new FloatMenuOption(faction.Name, () =>
					{
						PermitsCardUtility.selectedFaction = faction;
						PermitsCardUtility.selectedPermit = null;
					}, faction.def.FactionIcon, faction.Color));
				}

				Find.WindowStack.Add(new FloatMenu(options));
			}

			TooltipHandler.TipRegion(factionButtonRect, "SwitchFaction_Desc".Translate());
		}

		/// <summary>
		/// Draws a button for returning all permits.
		/// </summary>
		/// <param name="pawn">Selected pawn.</param>
		/// <param name="rect">Rectangle area in which the button will be placed.</param>
		public static void ReturnAllPermitsButton(Pawn pawn, Rect rect)
		{
			var returnAllPermitsRect = new Rect(rect.xMax - 180f, rect.y - 4f, 180f, 51f);
			var returnPermitsCost = TotalReturnPermitsCost(pawn);
			var favorCostArg = returnPermitsCost.Named("FAVORCOST");
			var favorArg = PermitsCardUtility.selectedFaction.def.royalFavorLabel.Named("FAVOR");

			if (Widgets.ButtonText(returnAllPermitsRect, "ReturnAllPermits".Translate()))
			{
				if (!pawn.royalty.PermitsFromFaction(PermitsCardUtility.selectedFaction).Any())
				{
					Messages.Message("NoPermitsToReturn".Translate(pawn.Named("PAWN")), new LookTargets(pawn),
						MessageTypeDefOf.RejectInput, false);
				}
				else if (pawn.royalty.GetFavor(PermitsCardUtility.selectedFaction) < returnPermitsCost)
				{
					Messages.Message(
						"NotEnoughFavor".Translate(favorCostArg, favorArg, pawn.Named("PAWN"),
							pawn.royalty.GetFavor(PermitsCardUtility.selectedFaction).Named("CURFAVOR")),
						MessageTypeDefOf.RejectInput);
				}
				else
				{
					Find.WindowStack.Add(Dialog_MessageBox.CreateConfirmation(
						(TaggedString) (string) "ReturnAllPermits_Confirm".Translate(8.Named("BASEFAVORCOST"),
							favorCostArg, favorArg, PermitsCardUtility.selectedFaction.Named("FACTION")),
						() => pawn.royalty.RefundPermits(8, PermitsCardUtility.selectedFaction), true));
				}
			}

			TooltipHandler.TipRegion(returnAllPermitsRect,
				"ReturnAllPermits_Desc".Translate(8.Named("BASEFAVORCOST"), favorCostArg,
					favorArg));
		}

		/// <summary>
		/// Show information about the currently selected faction.
		/// </summary>
		/// <param name="pawn">Selected pawn.</param>
		/// <param name="rect">Rectangle area in which the button will be placed.</param>
		public static void ShowSelectedFactionInfo(Pawn pawn, Rect rect)
		{
			var currentFaction = PermitsCardUtility.selectedFaction;
			var currentTitle = pawn.royalty.GetCurrentTitle(currentFaction);

			var selectedFactionTextureRect = new Rect(rect.xMax * LeftRectPercent + 30f, rect.y - 8f, 32f, 32f);
			var backupColor = GUI.color;
			GUI.color = new Color(currentFaction.Color.r, currentFaction.Color.g, currentFaction.Color.b,
				currentFaction.Color.a * GUI.color.a);
			GUI.DrawTexture(selectedFactionTextureRect, currentFaction.def.FactionIcon);
			GUI.color = backupColor;

			var selectedFactionNameRect = new Rect(selectedFactionTextureRect.xMax + 10f, rect.y - 4f, 360f, 32f);
			Widgets.Label(selectedFactionNameRect, currentFaction.Name);

			var selectedFactionInfoRect = new Rect((float) (rect.xMax - 380.0 - 4.0), rect.y - 4f, 360f, 55f);

			var infoLabel = "CurrentTitle".Translate() + ": ";
			if (currentTitle != null)
			{
				infoLabel += currentTitle.GetLabelFor(pawn).CapitalizeFirst();
			}
			else
			{
				var errorLogEntry = "[RoyaltyFramework] Pawn without title has accessed the Permits tab window.";
				Log.ErrorOnce(errorLogEntry, errorLogEntry.GetHashCode());
				infoLabel += "None".Translate();
			}

			infoLabel += "\n" + "UnusedPermits".Translate() + ": " + pawn.royalty.GetPermitPoints(currentFaction);
			if (!currentFaction.def.royalFavorLabel.NullOrEmpty())
			{
				infoLabel += "\n" + currentFaction.def.royalFavorLabel.CapitalizeFirst() + ": " +
					pawn.royalty.GetFavor(currentFaction);
			}

			Widgets.Label(selectedFactionInfoRect, infoLabel);
		}

		// 
		/// <summary>
		/// Displays information about the currently selected permit. Called DoLeftRect in the original code.
		/// </summary>
		/// <param name="pawn">Selected pawn.</param>
		/// <param name="rect">Rectangle area in which the button will be placed.</param>
		public static void ShowSelectedPermit(Pawn pawn, Rect rect)
		{
			const float y1 = 0.0f;
			var currentTitle = pawn.royalty.GetCurrentTitle(PermitsCardUtility.selectedFaction);

			var permit = PermitsCardUtility.selectedPermit;
			if (permit == null) return;

			GUI.BeginGroup(rect);
			Text.Font = GameFont.Medium;
			var permitLabelArea = new Rect(0.0f, y1, rect.width, 0.0f);
			Widgets.LabelCacheHeight(ref permitLabelArea, permit.LabelCap);
			Text.Font = GameFont.Small;
			var y2 = y1 + permitLabelArea.height;
			if (!permit.description.NullOrEmpty())
			{
				var permitDescriptionArea = new Rect(0.0f, y2, rect.width, 0.0f);
				Widgets.LabelCacheHeight(ref permitDescriptionArea, permit.description);
				y2 += permitDescriptionArea.height + 16f;
			}

			var label = "Cooldown".Translate() + ": ";
			label += "PeriodDays".Translate(permit.cooldownDays);

			var royalFavorLabel = PermitsCardUtility.selectedFaction.def.royalFavorLabel;
			if (permit.royalAid != null && permit.royalAid.favorCost > 0 && !royalFavorLabel.NullOrEmpty())
			{
				label += "\n" + "CooldownUseFavorCost".Translate(royalFavorLabel.Named("HONOR")).CapitalizeFirst() + ": " +
					permit.royalAid.favorCost;
			}

			if (permit.minTitle != null)
			{
				var taggedString = "RequiresTitle".Translate(permit.minTitle.GetLabelForBothGenders());
				var color = currentTitle == null || currentTitle.seniority < permit.minTitle.seniority
					? ColorLibrary.RedReadable
					: Color.white;
				label += "\n" + taggedString.Resolve().Colorize(color);
			}

			if (permit.prerequisite != null)
			{
				var taggedString = "UpgradeFrom".Translate(permit.prerequisite.LabelCap);
				var color = PermitsCardUtility.PermitUnlocked(permit.prerequisite, pawn)
					? Color.white
					: ColorLibrary.RedReadable;
				label += "\n" + taggedString.Resolve().Colorize(color);
			}

			var informationArea = new Rect(0.0f, y2, rect.width, 0.0f);
			Widgets.LabelCacheHeight(ref informationArea, label);
			var acceptButtonArea = new Rect(0.0f, rect.height - 50f, rect.width, 50f);
			if (permit.AvailableForPawn(pawn, PermitsCardUtility.selectedFaction) &&
				!PermitsCardUtility.PermitUnlocked(permit, pawn) &&
				Widgets.ButtonText(acceptButtonArea, "AcceptPermit".Translate()))
			{
				SoundDefOf.Quest_Accepted.PlayOneShotOnCamera();
				pawn.royalty.AddPermit(permit, PermitsCardUtility.selectedFaction);
			}

			GUI.EndGroup();
		}

		public static void DrawPermits(Pawn pawn, Rect rect)
		{
			var permitsRect = new Rect(rect);
			permitsRect.width *= LeftRectPercent;

			ShowSelectedPermit(pawn, permitsRect);

			// DoRightRect draws the set of permits that the selected faction can award.
			PermitsCardUtility.DoRightRect(new Rect(rect)
				{
					xMin = permitsRect.xMax + 10f
				},
				pawn);
		}

		/// <summary>
		/// Destructive Harmony prefix for PermitsCardUtility.DrawRecordsCard.
		/// </summary>
		/// <param name="rect">Area in which the permits window will be drawn.</param>
		/// <param name="pawn">Selected area.</param>
		/// <returns>Always false; the vanilla implementation is never executed.</returns>
		[HarmonyPrefix]
		static bool Prefix(Rect rect, Pawn pawn)
		{
			if (!ModLister.CheckRoyalty("Permit"))
			{
				return false;
			}

			var titles = pawn.royalty.AllTitlesInEffectForReading;
			if (!titles.Any())
			{
				return false;
			}

			if (!Enumerable.Any(titles, title => title.faction == PermitsCardUtility.selectedFaction))
			{
				PermitsCardUtility.selectedFaction = titles[0].faction;
			}

			rect.yMax -= 4f;

			SwitchFactionButton(titles, rect);
			ReturnAllPermitsButton(pawn, rect);
			ShowSelectedFactionInfo(pawn, rect);

			rect.yMin += 55f;

			DrawPermits(pawn, rect);

			// Do not run the original code.
			return false;
		}
	}
}