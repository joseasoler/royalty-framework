﻿using System.Reflection;
using Verse;

namespace RoyaltyFramework.Harmony
{
	/// <summary>
	/// Initialization of the Harmony patching of the mod.
	/// </summary>
	[StaticConstructorOnStartup]
	public class HarmonyInitialization
	{
		/// <summary>
		/// Initialization of the Harmony patching of the mod.
		/// </summary>
		static HarmonyInitialization()
		{
			var harmony = new HarmonyLib.Harmony("joseasoler.RoyaltyFramework");
			harmony.PatchAll(Assembly.GetExecutingAssembly());
		}
	}
}